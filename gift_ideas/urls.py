"""gift_ideas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static

from django.conf import settings

from app.views.category import CategoryView
from app.views.create import GiftCreateView
from app.views.detail import GiftDetailView
from app.views.index import IndexView
from app.views.login import LogoutView
from app.views.register import RegisterView

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += i18n_patterns(
    path(r'', IndexView.as_view(), name='app_index'),
    path('detail/<int:pk>', GiftDetailView.as_view(), name='app_gift_detail'),
    path('create/', GiftCreateView.as_view(), name='app_gift_create'),
    path(r'register/', RegisterView.as_view(), name='app_register'),
    path(r'category/', CategoryView.as_view(), name='app_category'),
    path(r'logout/', LogoutView.as_view()),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)