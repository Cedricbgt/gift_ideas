from django.contrib.auth.models import User
from django.db import models


# Create your models here.


class Category(models.Model):
    description = models.CharField(max_length=200, blank=True, null=True, default=None)

    def __str__(self):
        return f'{str(self.description)}'


class Gift(models.Model):
    title = models.CharField(max_length=200,
                             blank=False, null=False,
                             default='(no title)')
    description = models.TextField(blank=True,
                                   null=True,
                                   default=None)
    recipe = models.TextField(blank=True,
                              null=True,
                              default=None)
    categories = models.ManyToManyField(Category, related_name="gifts")
    photo = models.ImageField(blank=True, null=True, upload_to='covers/%Y/%m/%d/')

    def __str__(self):
        return '{} ({})'.format(self.title, '/'.join([str(c) for c in self.categories.all()]))


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    description = models.TextField(blank=True, null=True, default=None)

    def __str__(self):
        return f'{str(self.user)}'
