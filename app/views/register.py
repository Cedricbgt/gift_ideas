from django.contrib.auth.models import User
from django.views.generic import TemplateView, FormView

from app.form.register import RegisterForm
from app.models import Person


class RegisterView(FormView):
    template_name = 'register.html'
    form_class = RegisterForm

    def form_valid(self, form):
        email = form.cleaned_data['email']
        password_1 = form.cleaned_data['password_1']
        user = User.objects.create_user(email=email,
                                 password_2=password_1)
        Person.objects.create(user=user)
        Person.save()

        return super().form_valid(form)



