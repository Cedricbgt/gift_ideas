from django.db.models import Q
from django.views.generic import ListView

from app.models import Category, Gift


class CategoryView(ListView):
    template_name = 'category.html'
    model = Gift

    def get_queryset(self):
        query = self.request.GET.get('query')
        result = Gift.objects.filter(Q(title__icontains=query) | Q(description__icontains=query))
        return result