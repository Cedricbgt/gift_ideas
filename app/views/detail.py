from django.views.generic import DetailView

from app.models import Gift


class GiftDetailView(DetailView):
    template_name = 'giftDetails.html'
    model = Gift
