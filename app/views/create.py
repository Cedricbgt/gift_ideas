from django.urls import reverse
from django.views.generic import CreateView

from app.form.gift import GiftForm
from app.models import Gift


class GiftCreateView(CreateView):
    template_name = 'gift_create.html'
    model = Gift
    form_class = GiftForm

    def get_success_url(self):
        return reverse('app_index')
