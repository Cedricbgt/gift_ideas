from django.views.generic import ListView

from app.models import Gift


class IndexView(ListView):
    template_name = 'index.html'
    model = Gift

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Gift ideas'
        return result