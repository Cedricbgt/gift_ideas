from django.forms import models
from app.models import Gift


class GiftForm(models.ModelForm):
    class Meta:
        model = Gift
        fields = ['title', 'description', 'recipe', 'categories', 'photo']
