from django.contrib.auth.models import User
from django.forms import widgets, ModelForm
from django import forms


class RegisterForm(ModelForm):
    first_name = forms.CharField(max_length=200, min_length=4)
    last_name = forms.CharField(max_length=200, min_length=4)
    username = forms.CharField(max_length=200, min_length=4)
    email = forms.CharField(max_length=200, min_length=4)
    password_1 = forms.CharField(max_length=200, min_length=4,
                                 widget=widgets.PasswordInput(attrs={'placeholder': 'Pass 1'}))
    password_2 = forms.CharField(max_length=200, min_length=4,
                                 widget=widgets.PasswordInput)

    class Meta:
        model = User
        fields=['first_name','last_name','username', 'email', 'password_1', 'password_2']
