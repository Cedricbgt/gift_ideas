# Generated by Django 2.2.7 on 2020-01-30 08:58

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app', '0009_auto_20191213_2028'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gift',
            name='categories',
            field=models.ManyToManyField(related_name='gifts', to='app.Category'),
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('naissance', models.CharField(max_length=150)),
                ('description', models.TextField(blank=True, default=None, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
