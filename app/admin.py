from django.contrib import admin

# Register your models here.
from app.models import Gift, Category, Person

admin.site.register(Gift)
admin.site.register(Category)
admin.site.register(Person)
